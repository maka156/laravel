@extends('welcome')

@section('content')
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<h1>{{ $card->title }}</h1>

			<ul class="list-group">
				@foreach ($card->notes as $note)
					<li class="list-group-item">
						{{ $note->body }}
						{{ link_to_action('CardsController@index', $title = $note->user->name, $parameters = array(), $attributes = array('class' => 'pull-right')) }}
					</li>
				@endforeach
			</ul>

			<hr>


			<h1>Add a new note</h1>

			{!! Form::open(array('route' => array('addNote', $card->id))) !!}
				
				<div class="form-group">
					{!! Form::textarea('body', null, array(
						'required',
	              		'class'=>'form-control', 
	              		'placeholder'=>'Your message')) !!}
      			</div>

				<div class="form-group">
			    	{!! Form::submit('Add note', array('class'=>'btn btn-primary')) !!}
      			</div>

			{!! Form::close() !!}

			@if (count($errors))
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			@endif
		</div>
	</div>
@stop