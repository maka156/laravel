@extends('welcome')

@section('content')
	<h1>Edit the note</h1>

	{!! Form::open(array('route' => array('update', $note->id), 'method' => 'patch')) !!}
	
		
		<div class="form-group">
			{!! Form::textarea('body', null, array(
				'required', 
          		'class'=>'form-control', 
          		'placeholder'=>'Your message')) !!}
			</div>

		<div class="form-group">
	    	{!! Form::submit('Edit note', array('class'=>'btn btn-primary')) !!}
			</div>

	{!! Form::close() !!}
@stop