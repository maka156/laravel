<?php

namespace App\Http\Controllers;

Use App\Note;
Use App\Card;

use Illuminate\Http\Request;

use App\Http\Requests;

class NotesController extends Controller
{
    function addNote(Request $request, Card $card)
    {
    	//return $card;
    	//return $request->all();
    	/*
    	$card->notes()->create($request->all());
    	$card->notes()->create([
    		'body' => $request->body]
    		);*/

        $this->validate($request, ['body' => ['required', 'min:10']]);
        $note = new Note($request->all());
        $card->addNote($note, 1);

    	return back();
    }

    public function edit(Note $note)
    {
    	//dd('hit');
        return view('notes.edit', compact('note'));
    }

    public function update(Request $request, Note $note)
    {
        dd($request);
        $note->update($request->all());
        return back();
    }
}
