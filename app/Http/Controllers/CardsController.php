<?php

namespace App\Http\Controllers;

Use App\Card;

use Illuminate\Http\Request;

use App\Http\Requests;

class CardsController extends Controller
{
   public function index ()
   {
   		$cards = Card::all();
   		//return view('cards.index', compact('cards'));
         return $cards;
   }

   public function show(Request $request)
   {
   		$card = Card::find($request->get('id'));
         //return $card = Card::all();
         //return $card = Card::with('notes')->get();
         //$card = Card::with('notes.user')->find($id);

         //$card->load('notes.user');

   		//return view('cards.show', compact('card'));
         return $card;
   }

   public function store(Request $request)
   {
      $card = new Card;
      $card->title = $request->get('title');
      $card->save();
      return response($cards, 201);
   }

   public function update(Request $request)
   {
      $card = Card::find($request->get('id'));
      $card->title = $request->get('title');
      $card->save();
      return response($card, 201);
   }

   public function delete(Request $request)
   {
      $card = Card::find($request->get('id'));
      $card->delete();
      return response('Deleted.', 200);
   }
}