<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|


Route::get('/', function () {
    return view('welcome');
});
*/
Route::group(['middleware' => ['web']], function(){

	
	Route::get('/', 'CardsController@index');
	Route::get('cards/show', 'CardsController@show');
	Route::post('cards/store', 'CardsController@store');
	Route::patch('cards/update', ['as' => 'update', 'uses' => 'CardsController@update']);
	Route::delete('cards/delete', ['as' => 'delete', 'uses' => 'CardsController@delete']);

	Route::get('cards/{card}', 'CardsController@show');
	Route::post('cards/{card}/notes', ['as' => 'addNote', 'uses' => 'NotesController@addNote']);
	Route::get('notes/{note}', 'NotesController@edit');
	Route::patch('notes/{note}', ['as' => 'update', 'uses' => 'NotesController@update']);


});
